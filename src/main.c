#include <string.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct NAV_TimeUTC_payload {
    unsigned int iTOW;
    unsigned int tAcc;
    int nano;
    unsigned short year;
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char min;
    unsigned char sec;
    unsigned char valid;

}NAV_TimeUTC_payload;

typedef struct NAV_TimeUTC_packet {
    unsigned char sync_char_1;
    unsigned char sync_char_2;
    unsigned char class_id;
    unsigned short length;
    NAV_TimeUTC_payload payload;
    char ck_a;
    char ck_b;
}NAV_TimeUTC_packet;



NAV_TimeUTC_packet NAV_packet_creator(const void * buffer) {
    NAV_TimeUTC_packet packet;
    memcpy(&packet, buffer, 6);
    memcpy(&(packet.payload), buffer + 6, packet.length);
    memcpy(&(packet.ck_a), buffer + 6 + packet.length, 2);
    return packet;
}

void NAV_TimeUTC_TimeCheck(const unsigned char bitfield) {
    bool tow, week, utc = false;
    tow = bitfield &   0b00000001;
    week = (bitfield & 0b00000010)>>1;
    utc = (bitfield &  0b00000100)>>2;
    printf("%s", "tow validity: ");
    printf("%d\n", tow);
    printf("%s", "week validity: ");
    printf("%d\n", week);
    printf("%s", "utc validity: ");
    printf("%d\n", utc);
}

void NAV_packet_displayer(const NAV_TimeUTC_packet * packet) {
    printf("%s", "NAV Packet ---- \n");
    printf("%s", "length: ");
    printf("%u", packet->length);
    printf("%s", " bytes \n");
    printf("Date: %u %u %u, %u:%u:%u \n", packet->payload.year, packet->payload.month, packet->payload.day, packet->payload.hour, packet->payload.min, packet->payload.sec);
    NAV_TimeUTC_TimeCheck(packet->payload.valid);
}

void show_buffer(unsigned char * buffer, int length) {
    for (int i = 0; i < length; i++) {
        printf("0x%08x\n", buffer[i]);
        printf("%s", " ");
    }
}

int main(int argc, char * argv[]) {
    char buffer[64];
    //char * fname = argv[1];
    char * fname = "data/trame.bin";
    FILE * file = fopen(fname, "rb");
    if(!file) {
        return 1;
    }
    fread(buffer, 1, 64, file);
    NAV_TimeUTC_packet packet = NAV_packet_creator(buffer);

    NAV_packet_displayer(&packet);
    return 0;
}